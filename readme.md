<img src="https://raw.githubusercontent.com/joansalasoler/auale/assets/logo.png" alt="Aualé" width="258" height="90"/>
Play <b>Oware Mancala</b> against the computer
<hr>

What is it?
===========

Oware is a strategy board game of the Mancala family.

The goal of Oware is to capture the greatest amount of seeds as possible. To do so, players make moves in alternate turns until one of them has captured more than 24 seeds. The player who captured more seeds than the opponent when the game ends wins the match.

Play against one of the strongest computer players out there, watch it play alone while you learn or choose one of the provided strengths: easy, medium, hard or expert.

Analyze, record, tag and share your own Mancala games. Aualé can save your Oware matches in a portable and human-readable format.

![Demo](https://raw.githubusercontent.com/joansalasoler/auale/assets/auale-2.0.0.gif)

Implementation details:
-----------------------

* Multiplatform (GTK 3, Python 3, Java).
* Linux packaging scripts for debian, rpm, pacman and snap. MS Windows builds with py2exe.
* Uses SDL 2 mixer for sound effects.
* Translations provided into many languages.
* Strong [engine player](https://github.com/joansalasoler/aalina) written in Java.
* Play with the keyboard, mouse or gamepad.
* Windowed and fullscreen modes.

Many thanks to:
---------------

* My brother, Arnau Sala, for the sounds effects.
* Andrew Dabrowski, for his help with the pacman packages.
* Rémi Verschelde, for his help with the rpm packages.
* The amazing Launchpad translators for translating it.
* Everyone who submitted bug reports.

The Latest Version
==================

Information on the latest version of this software and its current
development can be found on https://auale.joansala.com/

Installation
============

Please see the INSTALL file.

Licensing
=========

Please see the COPYING file.
